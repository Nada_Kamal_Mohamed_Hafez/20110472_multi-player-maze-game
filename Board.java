package com.nada.maze;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.widget.ImageView;

public class Board extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_board);
		Resources res = getResources();
		Drawable drawable = res.getDrawable(R.drawable.ground);
	    drawable = res.getDrawable(R.drawable.wall);
	    drawable = res.getDrawable(R.drawable.player1);
	    drawable = res.getDrawable(R.drawable.player2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.board, menu);
		return true;
	}

}
