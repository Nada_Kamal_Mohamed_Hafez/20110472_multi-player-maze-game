package com.nada.maze;

import com.nada.maze.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.Btn1);
        btn.setOnClickListener(new OnClickListener(){
        	
		@Override
		public void onClick(View arg0) 
		{
            Intent intent = new Intent(MainActivity.this, Main_menu.class);
            startActivity(intent);
		}

        });
    }
}


