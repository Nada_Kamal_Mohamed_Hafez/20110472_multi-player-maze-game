package com.nada.maze;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Main_menu extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		Button btn = (Button) findViewById(R.id.Btn1);
        btn.setOnClickListener(new OnClickListener(){
        	
		@Override
		public void onClick(View arg0) 
		{
            Intent intent = new Intent(Main_menu.this, Board.class);
            startActivity(intent);
		}
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

}
